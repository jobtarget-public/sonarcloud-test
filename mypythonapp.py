def generate_pascals_triangle(rows):
    triangle = []


    for i in range(rows):
        row = [1]

        if i > 0:
            prev_row = triangle[-1]
            for j in range(1, i):
                row.append(prev_row[j - 1] + prev_row[j])
            row.append(1)
        triangle.append(row)

    return triangle

pass

# Intentional errors to be detected by SonarLint
rows = 5
print(generate_pascals_triangle(rows))