import unittest
import mypythonapp

class TestAppFunctions(unittest.TestCase):

    def test_generate_pascals_triangle(self):
        expected_triangle = [
            [1],
            [1, 1],
            [1, 2, 1],
            [1, 3, 3, 1],
            [1, 4, 6, 4, 1],
            [1, 5, 10, 10, 5, 1]
        ]
        self.assertEqual(mypythonapp.generate_pascals_triangle(6), expected_triangle)

    def test_generate_pascals_triangle_with_zero_rows(self):
        self.assertEqual(mypythonapp.generate_pascals_triangle(0), [])

if __name__ == '__main__':
    unittest.main()
